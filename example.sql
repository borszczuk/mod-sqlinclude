# MySQL dump 8.14
#
# Host: localhost    Database: test
#--------------------------------------------------------
# Server version	3.23.38-log

#
# Table structure for table 'email'
#

DROP TABLE IF EXISTS email;
CREATE TABLE email (
  data TEXT default NULL,
  ip varchar(32) default '192.168.0.1'
) TYPE=MyISAM;

#
# Dumping data for table 'email'
#

INSERT INTO email VALUES ('<VirtualHost 192.168.0.1>\nServerName test1.webnet.pl\nDocumentRoot /home/carlos/test1\n</VirtualHost>','192.168.0.1');
INSERT INTO email VALUES ('<VirtualHost 192.168.0.1>\nServerName test2.webnet.pl\nDocumentRoot /home/carlos/test2\n</VirtualHost>','192.168.0.1');

