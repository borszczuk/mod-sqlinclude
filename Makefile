##
##  $Id: Makefile,v 1.2 2003/05/22 21:10:40 carl-os Exp $
##
##  Makefile -- Build procedure for sample sqlinclude Apache module
##

#   the used tools
APXS		= apxs
APACHECTL	= apachectl

APACHE_LIBS	= /usr/lib/apache/1.3/


#   additional user defines, includes and libraries
LIB=-lmysqlclient

#   the default target
all: mod_sqlinclude.so

#   compile the DSO file
mod_sqlinclude.so: mod_sqlinclude.c
	$(APXS) -c $(DEF) $(INC) $(LIB) mod_sqlinclude.c


#   install target for Debian package building scripts
install-debian:	all
	install -d ${DESTDIR}/${APACHE_LIBS}
	install -m644 ./*.so ${DESTDIR}/${APACHE_LIBS}

deb: 
	dpkg-buildpackage -rfakeroot -uc -us
	lintian -i ../libapache-mod-sqlinclude*.deb


#   install the DSO file into the Apache installation
#   AND activate it in the Apache configuration
install: all
	$(APXS) -i -a -n 'sqlinclude' mod_sqlinclude.so

#   install the DSO file into the Apache installation
copy: all
	$(APXS) -i -n 'sqlinclude' mod_sqlinclude.so

#   cleanup local files
clean:
	-rm -f mod_sqlinclude.o mod_sqlinclude.so

#   simple test
#test: reload
#	lynx -mime_header http://localhost/sqlinclude

#   reload the module by installing and restarting Apache
reload: install restart

combo: all copy restart
import:
	mysql -p -u root test <apache_conf.sql

#   the general Apache start/restart/stop procedures
start:
	$(APACHECTL) start
restart:
	$(APACHECTL) restart
stop:
	$(APACHECTL) stop

